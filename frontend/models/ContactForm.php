<?php

namespace frontend\models;

use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class ContactForm extends Model
{
    public $name;
    public $phone;
    public $email;
    public $date;
    public $time;
    public $persons;
    public $message;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            // name, email, subject and body are required
            [['name', 'phone','email', 'date', 'time', 'persons'], 'required'],
            // email has to be a valid email address
            ['email', 'email'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'persons' => 'Number of persons'
        ];
    }

    /**
     * Sends an email to the specified email address using the information collected by this model.
     *
     * @param string $email the target email address
     * @return bool whether the email was sent
     */
    public function sendEmail($email)
    {
        return Yii::$app->mailer->compose()
            ->setTo($email)
            ->setFrom([$this->email => $this->name])
            ->setSubject('Table reservation')
            ->setTextBody('Name: ' . $this->name . ', Phone:' . $this->phone . ', E-mail:' . $this->email . ', Date:' . $this->date . ', Time' . $this->time . ', Number of persons' . $this->persons . ', Message' . $this->message)
            ->send();
    }
}
