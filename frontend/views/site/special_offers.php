 <!-- BEGIN MENU SECTION -->
 <section id="special-offers" class="section offers dark">

    <div class="container">
                <div class="voffset50"></div>
            <div class="jt_row jt_row-fluid row">
                <div class="col-md-12 jt_col column_container">
                    <h2 class="section-title">Lunch / Dinner / Special events / Corporative events</h2>
                </div>
                <div class="col-md-8 jt_col column_container"> 
                <img src="images/merry_xmas.jpg" />
                </div>   
                <div class="col-md-4 jt_col column_container"> 
                <div class="section-subtitle">
                    Make a reservation until 25.11.2018 and receive a 10% discount on the special offer.<br />
                    Offer valid from monday to thursday.
                </div>
                </div>
                <h2 class="text-center"><a href="/docs/xmas_menu.pdf" target="_blank">BANQUET MENU</a></h2>
                <div class="voffset50"></div>
                <h2 class="text-center"><a href="#reservations">RESERVATIONS</a></h2>
            </div>
    </div>
    <div class="voffset30"></div>
</section>
<!-- END MENU SECTION -->