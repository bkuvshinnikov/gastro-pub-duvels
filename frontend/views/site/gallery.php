 <!-- BEGIN GALLERY SECTION -->
 <section id="gallery" class="section gallery dark">
    <div class="jt_row jt_row-fluid row">
        <div class="col-md-12 jt_col column_container">
                <h2 class="section-title">Gallery</h2>
                <div class="col-md-6 col-md-offset-3 jt_col column_container">    
                    <div class="section-subtitle">
                        If a picture says a thousand words, then you can imagine how long it would take to describe all our selections. 
                    </div>
        </div>
                
        </div> 

        <div class="col-md-12 jt_col column_container">

            <nav class="primary">
                <ul>
                    <li><a href="#" data-filter="*" class="selected"><span>All photos</span></a></li>
                    <li><a href="#" data-filter=".dishes"><span>Dishes</span></a></li>
                    <li><a href="#" data-filter=".beer"><span>Beer</span></a></li>
                    <li><a href="#" data-filter=".interior"><span>Interior</span></a></li>
                    <li><a href="#" data-filter=".staff"><span>Staff</span></a></li>
                </ul>
            </nav>

            <div class="portfolio">

                <article class="entry dishes">
                    <img src="images/gallery/01.jpg" class="img-responsive" alt=""/>
                </article>
            
                <article class="entry dishes">
                    <img src="images/gallery/02.jpg" class="img-responsive" alt=""/>
                </article>
            
                <article class="entry dishes">
                    <img src="images/gallery/03.jpg" class="img-responsive" alt=""/>
                </article>
            
                <article class="entry interior">
                    <img src="images/gallery/08.jpg" class="img-responsive" alt=""/>
                </article>
            
                <article class="entry interior">
                    <img src="images/gallery/09.jpg" class="img-responsive" alt=""/>
                </article>
            
                <article class="entry interior">
                    <img src="images/gallery/10.jpg" class="img-responsive" alt=""/>
                </article>
            
                <article class="entry interior">
                    <img src="images/gallery/11.jpg" class="img-responsive" alt=""/>
                </article>
            
                <article class="entry beer">
                    <img src="images/gallery/12.jpg" class="img-responsive" alt=""/>
                </article>
                
                <article class="entry beer">
                    <img src="images/gallery/14.jpg" class="img-responsive" alt=""/>
                </article>
                
                <article class="entry staff">
                    <img src="images/gallery/20.jpg" class="img-responsive" alt=""/>
                </article>
                
                <article class="entry staff">
                    <img src="images/gallery/21.jpg" class="img-responsive" alt=""/>
                </article>
                
                <article class="entry beer">
                    <img src="images/gallery/18.jpg" class="img-responsive" alt=""/>
                </article>

            </div>
        </div> <!-- End .jt_col -->
    </div> <!-- End .jt_row -->
</section>
<!-- END GALLERY SECTION -->