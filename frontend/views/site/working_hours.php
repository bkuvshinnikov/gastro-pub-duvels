<!-- BEGIN TIMETABLE SECTION -->
<section id="timetable" class="section timetable parallax">
    <div class="container">
            <div class="jt_row jt_row-fluid row">
                <span class="column-divider"></span>
                <div class="col-md-12 jt_col column_container">
                    <h2 class="section-title"><span class="timetable-decorator"></span><span class="opening-hours">Opening Hours</span><span class="timetable-decorator2"></span></h2>
                </div> 
                <div class="col-md-12 jt_col column_container">    
                    <div class="section-subtitle">
                        Call For Reservations
                    </div>
                </div>
                
                <div class="col-md-3 col-md-offset-3 jt_col column_container">    
                    <div class="section-subtitle days">
                        Sunday to Tuesday
                    </div>
                    <div class="section-subtitle hours">
                        09:00<br>24:00
                    </div>
                </div>

                
                <div class="col-md-3 jt_col column_container">    
                    <div class="section-subtitle days">
                        Friday and Saturday
                    </div>
                    <div class="section-subtitle hours">
                        08:00<br>03:00
                    </div>
                </div>
                <div class="col-md-12 jt_col column_container">    
                    <div class="number">
                        RESERVATION NUMBER: 0842-5484214
                    </div>
                </div>

            </div>
    </div>
</section>
<!-- END TIMETABLE SECTION -->