<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

?>

<!-- BEGIN RESERVATIONS SECTION -->
<section id="reservations" class="section reservations">
    <div class="container">
            <div class="jt_row jt_row-fluid row">
                <div class="col-md-12 jt_col column_container">
                    <h2 class="section-title">Reservations</h2>
                </div> 
                <div class="col-md-12 jt_col column_container">
                    <!-- <h3>Make a Reservation</h3> -->
                    <!-- <h4><span>Open Hours</span></h4> -->
                    <div class="voffset50"></div>
                    <!-- <p><strong>Sunday to Tuesday</strong> 09.00 - 24:00 <strong>Friday and Sunday</strong> 08:00 - 03.00</p> -->
                    <h3 class="reservation-phone">+371 67 225 436   </h3>
                </div> 

                <?php $form = ActiveForm::begin(['id' => 'contact-form']); ?>
                <form action="site/contact" method="post" id="reservationform" class="contact-form">

                    <div class="col-md-10 col-md-offset-1 jt_col column_container">    
                        <p>Contact Details</p>
                        <?= $form->field($model, 'name')->textInput(['autofocus' => true]) ?>
                        <?= $form->field($model, 'phone')->textInput() ?>
                        <?= $form->field($model, 'email')->textInput() ?>
                    </div>
                    <div class="col-md-10 col-md-offset-1 jt_col column_container">    
                        <p>Book a table</p>
                        <?= $form->field($model, 'date')->textInput() ?>
                        <?= $form->field($model, 'time')->textInput() ?>
                        <?= $form->field($model, 'persons')->textInput() ?>
                    </div>

                    <div class="col-md-10 col-md-offset-1 jt_col column_container">
                        <p>Wishes</p>    
                        <?= $form->field($model, 'message')->textArea() ?>
                    </div>
                    <div class="col-md-4 col-md-offset-4 jt_col column_container">
                    <div class="formSent"><strong>Your Message Has Been Sent!</strong> Thank you for contacting us.</div>     
                        <?= Html::submitButton('Make reservation', ['class' => 'button center']) ?>
                    </div>

                <?php ActiveForm::end(); ?>
                <div class="col-md-12 jt_col column_container">
                <div class="voffset60"></div>
                    <!-- <div class="ornament"></div> -->
                </div>
            </div>
    </div>
</section>
<!-- END RESERVATIONS SECTION-->