<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;

?>

<section id="gift-cards" class="section">
    <div class="container">
            <div class="jt_row jt_row-fluid row">
                <div class="col-md-12 jt_col column_container">
                    <h2 class="section-title">Gift cards</h2>
                </div>

                <div class="col-md-6 jt_col column_container">

                <?php $form = ActiveForm::begin(['id' => 'gift-cards-form', 'method' => 'post', 'action' => Url::to(['site/cards'])]) ?>

                    <div class="col-md-10 col-md-offset-1 jt_col column_container">
                        <?= $form->field($model, 'name')->textInput(['autofocus' => true]) ?>
                        <?= $form->field($model, 'phone')->textInput() ?>
                        <?= $form->field($model, 'email')->textInput() ?>
                        <?= $form->field($model, 'amount')->textInput() ?>
                        <span>The minimum amount - 30 euros</span>
                    </div>
                    <div class="col-md-4 col-md-offset-4 jt_col column_container">
                    <div class="formSent"><strong>Your Message Has Been Sent!</strong> Thank you for contacting us.</div>     
                        <?= Html::submitButton('Order', ['class' => 'button center']) ?>
                    </div>

                <?php ActiveForm::end(); ?>
                </div>

                <div class="col-md- jt_col column_container">
                    <img src="images/gift_cards.jpg">
                </div>
                <div class="voffset60"></div>
            </div>
    </div>
</section>