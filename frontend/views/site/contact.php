<!-- BEGIN CONTACT SECTION -->
<section id="contact" class="section contact dark">
    <div class="container">
        <div class="jt_row jt_row-fluid row">

            <div class="col-md-12 jt_col column_container">
                <h2 class="section-title">Contact</h2>
            </div>
            <div class="col-md-6 col-md-offset-3 jt_col column_container">
                <div class="section-subtitle">
                    W325 State Road 123 Mondovi, WI  (Wisconsin)   98746-54321
                </div>
            </div>
                <form action="mail.php" method="post" id="contactform" class="contact-form">
                        <div class="col-md-6 jt_col column_container">    
                            <input type="text" id="name" name="name" class="text name required" placeholder="Name" >
                            <input type="email" id="email" name="email" class="tex email required" placeholder="Email" >
                            <input type="text" id="subject" name="subject" placeholder="Subject" >
                        </div>

                        <div class="col-md-6 jt_col column_container">    
                                <textarea id="message" name="message" class="text area required" placeholder="Message" rows="10"></textarea>
                        </div>

                        <div class="col-md-4 col-md-offset-4 jt_col column_container">  
                        <div class="formSent"><strong>Your Message Has Been Sent!</strong> Thank you for contacting us.</div>   
                        <input type="submit" class="button contact center" value="Submit" >
                        </div>
                        
                    </form>
            <div class="voffset100"></div>
        </div>
            <div class="voffset50"></div>
    </div>
    
</section>
<!-- END CONTACT SECTION -->