<!-- BEGIN MAP SECTION -->
<section id="location" class="">
    <div class="col-md-12 jt_col column_container">
        <h2 class="section-title">Location</h2>
    </div> 
    <div class="col-md-12 jt_col column_container">
        <h3 class="reservation-phone">Meistaru ielā 10/12 Riga, Latvia</h3>
        <h3 class="reservation-phone">info@gastropubriga.lv</h3>
        <h3 class="reservation-phone">+371 67 225 436</h3>
        <div class="voffset50"></div>
    </div> 
    <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&key=AIzaSyCv-0eVCQrwle-PpkBANIYYCIV5JXq_Zt0"></script>
    <div class="map-content">
        <div class="wpgmappity_container inner-map" id="wpgmappitymap"></div>
    </div>
</section>
<!-- END MAP SECTION -->