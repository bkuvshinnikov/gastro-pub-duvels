<?php

    use yii\helpers\Html;

    /* @var $this yii\web\View */

    $this->title = 'Duvel\'s Gastro Pub';

?>

<?= $this->render('slider') ?>

<?= $this->render('giftCards', ['model' => $giftCardsForm]) ?>

<?= $this->render('menu') ?>

<?= $this->render('gallery') ?>

<?= $this->render('reservations', ['model' => $reservationsForm]) ?>
       
<?= $this->render('location') ?>