<!-- BEGIN ABOUT SECTION -->
<section id="about" class="section about">
    <div class="container">
        <div class="jt_row jt_row-fluid row">
            <div class="col-md-12 jt_col column_container">
                <h2 class="section-title">About Us</h2>
            </div> 
            <div class="col-md-6 col-md-offset-3 jt_col column_container">    
                <div class="section-subtitle">
                    We love restaurants as much as you do. That’s why we’ve been helping them fill tables since 1999. Welcome to elixir restaurant 
                </div>
            </div>


            <div class="col-md-6 jt_col column_container">
                <h2 class="heading font-smoothing">The History</h2>
                <p class="text">
                    The <strong>History of Kitchens</strong> and Cooks gives further intimation on Mr Boulanger 
                    usual menu, stating confidently that "Boulanger served salted poultry and 
                    fresh eggs, all presented without a tablecloth on small marble tables". 
                    Numerous commentators have also referred to the supposed restaurant 
                    owner's eccentric habit of touting for custom outside his establishment, 
                    dressed in aristocratic fashion and brandishing a sword
                </p>
                <p class="text">
                    According to Miss Spang, there is not a shred of evidence for any of it. 
                    She said: These legends just get passed on by hearsay and then spiral 
                    out of control. Her interest in <strong>Boulanger</strong> dates back to a history of 
                    food seminar in Paris in the mid-1990s
                </p>
                
                <div class="ornament"></div>
            </div>  

            <div class="col-md-6 jt_col column_container">
            <div class="voffset40"></div>
                <div id="owl-about" class="owl-carousel">
                        <div class="item"><img src="images/about01.jpg" alt=""></div>
                        <div class="item"><img src="images/about02.jpg" alt=""></div>
                        <div class="item"><img src="images/about03.jpg" alt=""></div>
                </div>
            </div> 
        </div>
        <!-- <div class="jt_row jt_row-fluid row">
            <div class="col-md-6 jt_col column_container">
                <div class="voffset10"></div>
                <div id="owl-about2" class="owl-carousel">
                        <div class="item"><img src="images/about04.jpg" alt=""></div>
                        <div class="item"><img src="images/about05.jpg" alt=""></div>
                        <div class="item"><img src="images/about06.jpg" alt=""></div>
                </div>
            </div>  



            <div class="col-md-6 jt_col column_container">

                <p class="text">
                    The <strong>History of Kitchens</strong> and Cooks gives further intimation on Mr Boulanger 
                    usual menu, stating confidently that "Boulanger served salted poultry and 
                    fresh eggs, all presented without a tablecloth on small marble tables". 
                    Numerous commentators have also referred to the supposed restaurant 
                    owner's eccentric habit of touting for custom outside his establishment, 
                    dressed in aristocratic fashion and brandishing a sword
                </p>
                <p class="text">
                    According to Miss Spang, there is not a shred of evidence for any of it. 
                    She said: These legends just get passed on by hearsay and then spiral 
                    out of control. Her interest in <strong>Boulanger</strong> dates back to a history of 
                    food seminar in Paris in the mid-1990s
                </p>
            </div> 
        </div> -->

    </div>
    
</section>
<!-- END ABOUT SECTION -->