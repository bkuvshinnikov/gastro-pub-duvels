<!-- BEGIN GALLERY SECTION -->
<section id="menu" class="section">
    <div class="jt_row jt_row-fluid row">
        <div class="col-md-12 jt_col column_container">
            <h2 class="section-title">Menu</h2>
            <!-- <div class="col-md-6 col-md-offset-3 jt_col column_container">    
                <div class="section-subtitle">
                    Menu will be available in the near future
                </div>
            </div> -->
        </div> 

        <div class="col-md-12 jt_col column_container">
            <h2 class="text-center" style="margin-bottom: 30px;">Food</h2>
            <nav class="primary">
                <ul>
                    <li><a href="/docs/beer_snacks.pdf" target="_blank"><span>Beer snacks</span></a></li>
                    <li><a href="/docs/hot_snacks.pdf" target="_blank"><span>Hot Snacks & Salads</span></a></li>
                    <li><a href="/docs/snacks.pdf" target="_blank"><span>Snacks</span></a></li>
                    <li><a href="/docs/soups.pdf" target="_blank"><span>Soups & Pastas</span></a></li>
                    <li><a href="/docs/ribs.pdf" target="_blank"><span>BBQ Ribs & Burgers</span></a></li>
                </ul>
            </nav>
            <nav class="primary">
                <ul>
                    <li><a href="/docs/main.pdf" target="_blank"><span>Main</span></a></li>
                    <li><a href="/docs/grill.pdf" target="_blank"><span>Cooked on coals</span></a></li>
                    <li><a href="/docs/mussels.pdf" target="_blank"><span>Mussels and fish courses</span></a></li>
                    <li><a href="/docs/sides_and_deserts.pdf" target="_blank"><span>Sides, sauses & deserts</span></a></li>
                    <li><a href="/docs/pizza.pdf" target="_blank"><span>Pizza</span></a></li>
                </ul>
            </nav>
            <h2 class="text-center" style="margin-bottom: 30px;">Drinks</h2>
            <nav class="primary">
                <ul>
                    <li><a href="/docs/bottled_beer.pdf" target="_blank"><span>Bottled beer</span></a></li>
                    <li><a href="/docs/bottled_beer_2.pdf" target="_blank"><span>Bottled beer 2</span></a></li>
                    <li><a href="/docs/draft_beer.pdf" target="_blank"><span>Draft beer</span></a></li>
                    <li><a href="/docs/strong.pdf" target="_blank"><span>Strong</span></a></li>
                    <li><a href="/docs/cocktails.pdf" target="_blank"><span>Cocktails</span></a></li>
                    <li><a href="/docs/wine.pdf" target="_blank"><span>Wine</span></a></li>
                    <li><a href="/docs/non-alcoholic.pdf" target="_blank"><span>Non-alcoholic</span></a></li>
                </ul>
            </nav>
        </div> 
    </div> 
</section>

            