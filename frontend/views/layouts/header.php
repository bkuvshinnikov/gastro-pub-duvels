 <!-- BEGIN HEADER -->
 <header id="header" role="banner">
    <div class="jt_row container">
        <div class="navbar-header page-scroll">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-main-collapse">
                <i class="fa fa-bars"></i>
                </button>
            <a class="navbar-brand normal logo" href="#home-slider"><div class="logo_elixir"></div></a>
            <a class="navbar-brand mini" href="#home-slider"><div class="logo_elixir dark"></div></a>
            <a class="navbar-brand mini darker" href="#home-slider"><div class="logo_elixir dark"></div></a>
        </div>

        <!-- BEGIN NAVIGATION MENU-->
        <nav class="collapse navbar-collapse navbar-right navbar-main-collapse" role="navigation">
            <ul id="nav" class="nav navbar-nav navigation">
                <li class="page-scroll menu-item"><a href="#gift-cards">Gift cards</a></li>
                <li class="page-scroll menu-item"><a href="#menu">Menu</a></li>
                <li class="page-scroll menu-item"><a href="#gallery">Gallery</a></li>
                <li class="page-scroll menu-item"><a href="#reservations">Reservations</a></li>
                <li class="page-scroll menu-item"><a href="#location">Location</a></li>
                <li class="menu-item"><a href="https://wolt.com/en/lva/riga/restaurant/duvels-gastro-pub" target="_blank">Delivery</a></li>
            </ul>
        </nav>
        <!-- EN NAVIGATION MENU -->
    </div>
</header>
<!-- END HEADER -->