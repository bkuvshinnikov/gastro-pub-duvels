<!-- BEGIN FOOTER -->
<footer id="footer" class="section" role="contentinfo">
    <div class="container">
        <ul class="social">
            <li><a href="https://www.instagram.com/gastro_pub_riga/" target="_blank" class="icon tw"><i class="fa fa-instagram"></i></a></li>
            <li><a href="https://www.facebook.com/gastro.pub.riga" class="icon fb"><i class="fa fa-facebook"></i></a></li>
        </ul>
    </div>
</footer>
<!-- END FOOTER -->