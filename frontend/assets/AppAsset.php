<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/bootstrap.min.css',
        'css/font-awesome.min.css',
        'css/elixir.css',
        'css/YTPlayer.css',
        'js/owl-carousel/owl.carousel.css',
        'js/owl-carousel/owl.theme.css',
        'js/owl-carousel/owl.transitions.css',
        'css/site.css'
    ];
    public $js = [
        'js/modernizr.custom.js',
        'js/jquery.js',
        'js/classie.js',
        'js/pathLoader.js',
        'js/owl-carousel/owl.carousel.min.js',
        'js/jquery.inview.js',
        'js/jquery.nav.js',
        'js/jquery.mb.YTPlayer.js',
        'js/jquery.form.js',
        'js/jquery.validate.js',
        'js/bootstrap.min.js',
        'js/default.js',
        'js/plugins.js',
        'js/jquery.isotope.min.js',
        'js/jquery.prettyPhoto.js',
        'js/jquery.swipebox.js'
    ];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
